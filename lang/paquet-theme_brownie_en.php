<?php
    // This is a SPIP language file  --  Ceci est un fichier langue de SPIP

    if (!defined('_ECRIRE_INC_VERSION')) return;

    $GLOBALS[$GLOBALS['idx_lang']] = array(

        // T
        'theme_brownie_nom' => "Theme Brownie",
        'theme_brownie_description' => "Brownie is a responsive theme for SPIP 3 well suited for news sites and blogs",
        'theme_brownie_slogan' => "Responsive theme for the creation of news sites and blogs."
    );

?>